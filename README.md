# Welcome to Neurodyne Designer Environment

We at Neurodyne strive to integrate the latest technology, increase the automation and the design abstraction level to push our productivity to the absolute maximum.
All new cores are designed in Chisel. 
We use System Verilog for backward compatibility, Verilator for SoC simulations and Xilinx FPGA for prototyping.

ASIC flow is 100% open source as well. We have access to a range of technolgies, from 180nm down to 7nm.

Common environment setup for working with Neurodyne projects

## Ubuntu Setup 

```bash
sudo apt-get update; sudo apt-get dist-upgrade; sudo apt-get autoremove
```

## JVM Setup - Oracle Java 11

### Ubuntu 16.04
```bash
sudo add-apt-repository ppa:linuxuprising/java
sudo apt update
sudo apt-get install oracle-java11-installer
```

### Ubuntu 18.04
```bash
sudo apt-get install oracle-java11-set-default
```

## Verify Java Installation
```bash
java -version
java version "11.0.2" 2019-01-15 LTS
Java(TM) SE Runtime Environment 18.9 (build 11.0.2+9-LTS)
Java HotSpot(TM) 64-Bit Server VM 18.9 (build 11.0.2+9-LTS, mixed mode)
```

## Setup SBT 1.2.8

```bash
echo "deb https://dl.bintray.com/sbt/debian /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823
sudo apt-get update
sudo apt-get install sbt
```

## Verilator setup - follow home page [instructions](https://www.veripool.org/projects/verilator/wiki/Installing)

## Chisel setup 

Checkout this project and run the following:

```bash
./upd.sh
./pub.sh
```
This will remove old copies of the Berkeley stuff, checkout snapshots, build required projects and publish locally for reuse in all projects.