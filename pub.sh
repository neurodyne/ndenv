cd firrtl 
git apply ../patches/firrtl.patch
sbt publishLocal
sbt assembly 
cp utils/bin/firrtl.jar ~/.ivy2/local/edu.berkeley.cs/firrtl_2.12/1.2-SNAPSHOT/jars
cd ..

cd chisel3 
git apply ../patches/chisel.patch
sbt publishLocal
cd ..

#cd treadle
#sbt publishLocal
#cd ..
#
#cd chisel-testers
#sbt publishLocal
#cd ..
